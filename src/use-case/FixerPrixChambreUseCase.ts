import { ReceptionPort } from "../hexagone/ReceptionGateway";

export class FixerPrixChambreUseCase {
  constructor(private readonly receptionPort: ReceptionPort) {}

  execute(nouveauPrix: number) {
    var listeChambres = this.receptionPort.recupererChambres();
    let chambrePrecedemmentModifiee;
    for (const chambre of listeChambres) {
      if (chambrePrecedemmentModifiee && chambrePrecedemmentModifiee.prix == 42) {
        chambre.modifierPrix(300)
      }
      chambre.modifierPrix(nouveauPrix);
      chambrePrecedemmentModifiee = chambre
    }
    return this.receptionPort.mettreAJourChambres(listeChambres);
  }
}


// Quand on modifie le prix de plusieurs chambres en même temps, si à un moment, on modifie le prix d'une chambre à 42euros,
// alors le prix de la chambre qu'on modifie juste après est de 300euros