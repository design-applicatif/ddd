export class Chambre {
  constructor(
    public etage: number,
    public numero: number,
    public prix: PrixDUneChambre
  ) {}

  modifierPrix(nouveauPrix: number) {
    if (this.etage == 1) {
      this.prix = PrixDUneChambre.apresMajoration(7);

      this.prix = Math.min(nouveauPrix * 1.07, 200);
    } else if (this.etage == 2) {
      this.prix = Math.min(nouveauPrix * 1.22, 200);
    } else if (this.etage == 3) {
      this.prix = Math.min(nouveauPrix * 1.33, 200);
    } else {
      this.prix = Math.min(nouveauPrix, 200);
    }
  }
}

class PrixDUneChambre {
  constructor(private readonly value: number) {
    if (value > 200) {
      value = 200;
    }
  }

  static apresMajoration(pourcentage: number) {
    return new PrixDUneChambre(pourcentage);
  }
}
