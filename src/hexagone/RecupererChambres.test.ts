import { describe } from "node:test";
import { Chambre } from "./Chambre";
import { ReceptionTestAdapter } from "./ReceptionTestAdapter";
import { RecupererChambresUseCase } from "../use-case/RecupererChambreUseCase";

describe("Récupérer chambres", () => {
  describe(`Quand il n'y a pas de chambres dans l'hôtel`, () => {
    it("doit retourner une liste vide", () => {
      // given
      const hotel = new RecupererChambresUseCase(new ReceptionTestAdapter([]));
      // when
      const listeDesChambres = hotel.execute();
      // then
      expect(listeDesChambres).toEqual([]);
    });
  });

  describe(`Quand il y a une chambre dans l'hôtel`, () => {
    it("doit retourner une liste avec une chambre", () => {
      // given
      const presenter = new PresenterString();
      const hotel = new RecupererChambresUseCase(
        new ReceptionTestAdapter([new Chambre(0, 1, 50)])
      );
      const uneChambre = new Chambre(0, 1, 50);
      // when
      hotel.execute(presenter);
      // then
      expect(presenter.presenteLesChambresEnString()).toEqual([
        "chambre numéro 1, à l'étage 0 à 50 euros",
      ]);
    });
  });
});
